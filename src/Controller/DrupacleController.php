<?php

namespace Drupal\drupacle\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\drupacle\Entity\DrupacleConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class DrupacleController.
 */
class DrupacleController extends ControllerBase {
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function drupalConnectionCallback(DrupacleConnectionInterface $drupacle_connection) {
    $connectionId = $drupacle_connection->get('id');
    $db_username = $drupacle_connection->get('username');
    $db_password = $drupacle_connection->get('password');
    $getHost = $drupacle_connection->getHostWithPortAndService();

    $oracleDB = oci_connect($db_username, $db_password, $getHost);
    if ($oracleDB) {
      $response[$connectionId] = $oracleDB;
    }
    else {
      $error = oci_error();
      $response[$connectionId] = $error['message'];
    }
    return $response;
  }

}
